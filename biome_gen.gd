extends Control

class Tile extends Node:
	var x: int
	var y: int
#	var color: Color # array of ints for byte ops
	var height_type: int
	var height_value: float
	var heat_value: float
	var precipitation: float
	var is_land: bool
	var collidable: bool
	var flood_filled: bool
	var left: Tile
	var right: Tile
	var top: Tile
	var bottom: Tile
	var bitmask: int
#	func _init():
	func check_neighbors(var map, width):
		if x > 0:
			left = map[x - 1][y]
		else: left = null
		if x < width - 1:
			right = map[x + 1][y]
		else: right = null
		if y > 0:
			top = map[x][y - 1]
		else: top = null
		if y < width - 1:
			bottom = map[x][y + 1]
		else: bottom = null
	
	func update_bitmask(value: float = 0.15):
		var return_value = 0.00
		var count = 0
		if top is Tile:
			if top.height_type == height_type:
				count += 1
		elif top == null:
			count += 1
		if right is Tile:
			if right.height_type == height_type:
				count += 2
		elif right == null:
			count += 2
		if bottom is Tile:
			if bottom.height_type == height_type:
				count += 4
		elif bottom == null:
			count += 4
		if left is Tile:
			if left.height_type == height_type:
				count += 8
		elif left == null:
			count += 8
		bitmask = count
		if bitmask < 15:
			return_value -= value
		return return_value

enum ELEVATION_TYPES {
	DeepestWater = 0
	DeepWater = 1
	ShallowWater = 2
	Sand = 3
	Grass = 4
	Forest = 5
	Rock = 6
	Snow = 7
}
var ELEVATION_VALUES: Dictionary = {
	DeepWater = 0.575,
	ShallowWater = 0.52,
	Sand = 0.5,
	Grass = 0.49,
	Forest = 0.45,
	Rock = 0.38,
	Snow = 0.31
}
var EQUATORS: Dictionary = {
	Vertical = 0,
	Horizontal = 1,
}
var HEIGHT_COLORS: Dictionary = {
	DeepestWater = rgb(33, 70, 122),
	DeepWater = rgb(63, 100, 152),
	ShallowWater = rgb(176, 230, 244),
	Sand = rgb(240, 240, 64),
	Grass = rgb(50, 220, 20),
	Forest = rgb(16, 160, 0),
	Rock = rgb(127, 127, 127),
	Snow = rgb(255, 255, 255)
}
var WATER_COOLING: Array = [
	-0.125,
	-0.1,
	-0.075,
]
var HEAT_COLORS: Dictionary = {
	Low = rgb(130, 247, 255),
	High = rgb(255, 65, 65)
}
var PRECIP_COLORS: Dictionary = {
	Low = rgb(103, 0, 31),
	Middle = rgb(247, 247, 247),
	High = rgb(5, 48, 97)
}

var map: Array = []

export var width: int = 512
export var height: int = width
var tiles_complete: int = 0
var generating: bool = false
var load_steps: int = 5
var tiles_total: int = width * height * load_steps
# warning-ignore:integer_division
var screen_height: int = 800
var max_size: int = 512
var world_scale: Vector2 = Vector2(max_size / float(width),max_size / float(width))
var image_scale: Vector2 = Vector2(ceil(screen_height / float(width * 2)),ceil(screen_height / float(width * 2)))

#var thread

var simplex_noise:OpenSimplexNoise = OpenSimplexNoise.new()

var humid_alpha: int = 100
var noise_period: float = 32.0
var noise_persistence: float = 0.37
var noise_lacunarity: float = 2.04
var noise_octaves: int = 6
var world_equator: int = EQUATORS.Horizontal

var image_samples_path: String = "res://image_samples/"
var gradient_path: String = "data/gradient/"
var gradient_width: int = 1024

var mode: int = 0

var check_progress_bar_step: float = 0.1
var check_progress_bar_tick: float = 0.0

#debug
var tiles_instanced: int = 0

onready var height_map_node: TextureRect = Helper.get_correct_node(self, "BGHeightMap", true)
onready var heat_map_node: TextureRect = Helper.get_correct_node(self, "BGHeatMap", true)
onready var precip_map_node: TextureRect = Helper.get_correct_node(self, "BGPrecipMap", true)
onready var progress_bar: ProgressBar = Helper.get_correct_node(self, "BGProgressBar", true) as ProgressBar
onready var side_by_side_container: HBoxContainer = Helper.get_correct_node(
	self, "BGSideBySideContainer", true) as HBoxContainer
onready var overlay_container: Control = Helper.get_correct_node(
	self, "BGOverlayContainer", true) as Control
onready var switch_mode_button: Button = Helper.get_correct_node(
	self, "BGSwitchModeButton", true) as Button

func _ready():
# warning-ignore:return_value_discarded
	set_process(true)
	height_map_node.hide()
	heat_map_node.hide()
	precip_map_node.hide()
	switch_mode_button.hide()
#	start_generation()
	pass

func _process(delta):
	if Input.is_action_just_released("quit"):
		get_tree().quit()
		pass
	if not generating: return
	if check_progress_bar_tick < check_progress_bar_step:
		check_progress_bar_tick += delta
	else:
		progress_bar.value = stepify(float(tiles_complete) / float(tiles_total) * 100.0, 0.01)
		check_progress_bar_tick = 0.0
	
	

func start_generation(world_size: Vector2 = Vector2(width, height)):
	$Generate.disable()
	if mode == 1: switch_mode_pressed()
	height_map_node.hide()
	heat_map_node.hide()
	precip_map_node.hide()
	switch_mode_button.hide()
	progress_bar.label.text = ""
	progress_bar.value = 0.00
	progress_bar.fade_in()
	tiles_complete = 0
	var thread = Thread.new()
	if thread.is_active():
		call_deferred("start_generation")
		return
	else:
		width = int(world_size.x)
		height = width
		generating = true
		thread.start(self, "generate_map", thread)
	pass


func noise_to_type(pixel: float):
	if pixel < ELEVATION_VALUES.Snow:
		return ELEVATION_TYPES.Snow
	if pixel < ELEVATION_VALUES.Rock:
		return ELEVATION_TYPES.Rock
	if pixel < ELEVATION_VALUES.Forest:
		return ELEVATION_TYPES.Forest
	if pixel < ELEVATION_VALUES.Grass:
		return ELEVATION_TYPES.Grass
	if pixel < ELEVATION_VALUES.Sand:
		return ELEVATION_TYPES.Sand
	if pixel < ELEVATION_VALUES.ShallowWater:
		return ELEVATION_TYPES.ShallowWater
	if pixel < ELEVATION_VALUES.DeepWater:
		return ELEVATION_TYPES.DeepWater
	else:
		return ELEVATION_TYPES.DeepestWater
	pass

func confirm_internal_data():
	
	screen_height = OS.window_size.y
	tiles_total = (width * height) * load_steps
	world_scale = Vector2(max_size / float(width),max_size / float(width))
	image_scale = Vector2(ceil(screen_height / float(width * 2)),ceil(screen_height / float(width * 2)))
	pass

func generate_map(_thread : Thread, equator = world_equator, octaves: int = noise_octaves, period: float = noise_period, 
	persistence: float = noise_persistence, lacunarity: float = noise_lacunarity):
	#start
	confirm_internal_data()
	
	
	### Load Step 1
	progress_bar.label.text = "Creating and loading temporary Helpers..."
	var equator_path
	match equator:
		EQUATORS.Horizontal:
			equator_path = "horizontal/"
		EQUATORS.Vertical:
			equator_path = "vertical/"
	
	
	var precip_color_gradient = Gradient.new()
	precip_color_gradient.set_color(0, PRECIP_COLORS.Low)
	precip_color_gradient.set_offset(0, 0.25)
	precip_color_gradient.set_color(1, PRECIP_COLORS.Middle)
	precip_color_gradient.set_offset(1, 0.5)
	precip_color_gradient.add_point(1, PRECIP_COLORS.High)
	precip_color_gradient.set_offset(2, 0.75)

	for i in precip_color_gradient.offsets:
		print(i)
	
	var file = File.new()
	var gradient_files = list_files_in_directory(image_samples_path + gradient_path + equator_path)
	var path = image_samples_path + gradient_path + equator_path + gradient_files[randi() % gradient_files.size()]
	# load gradient array file
	if file.open(path, File.READ) != OK:
		push_error("Failed to open file: " + path)
	var heat_gradient = file.get_var() as Array
	file.close()
	var gradient_scale = float(gradient_width) / float(width)
	if gradient_scale > 1:
		heat_gradient = shrink_2d_array(heat_gradient, gradient_scale)
	

	var new_img_height = Image.new()
	var new_img_heat = Image.new()
	var new_img_precip = Image.new()
	var img_tex_height = ImageTexture.new()
	var img_tex_heat = ImageTexture.new()
	var img_tex_precip = ImageTexture.new()
	new_img_height.create(width, height, true, Image.FORMAT_RGBA8)
	new_img_heat.create(width, height, true, Image.FORMAT_RGBA8)
	new_img_precip.create(width, height, true, Image.FORMAT_RGBA8)
	
	
#	tiles_complete += width * height
#	progress_bar.value = stepify(float(tiles_complete) / float(tiles_total) * 100.0, 0.01)
	### END Load Step 1
	
#	if map.size() > 0:
#		progress_bar.label.text = "Clearing Cache..."
#		clear_map(map)
	
	### Load Step 1
	
	progress_bar.label.text = "Propagating Data Array..."
	map = create_2d_array(width, height)
	
	### END Load Step 1
	
	### Load Step 2
	
	progress_bar.label.text = "Generating Simplex Noise..."
	simplex_noise.octaves = octaves
	simplex_noise.period = period
	if int(world_scale.x) != 1:
		simplex_noise.period = period / (world_scale.x * 0.25)
	simplex_noise.persistence = persistence
	simplex_noise.lacunarity = lacunarity
	simplex_noise.seed = randi()
#	simplex_noise.seed = 0
	var height_map = simplex_noise.get_seamless_image(width)
	simplex_noise.seed = randi()
	var precip_map = simplex_noise.get_seamless_image(width)
	
	tiles_complete += width * height
#	progress_bar.value = stepify(float(tiles_complete) / float(tiles_total) * 100.0, 0.01)
	progress_bar.label.text = "Converting Noise to Heightmap..."
	height_map.lock()
	precip_map.lock()
	new_img_height.lock()
	new_img_heat.lock()
	new_img_precip.lock()
	
	tiles_complete += width * height
	### END Load Step 2
	
	### Load Step 3
	#Generate terrain from OpenSimplex, then sort the tiles into 2 groups, Land and Water
	for x in range(width):
		for y in range(height):
			var noise_sample = height_map.get_pixel(x, y).v
			map[x][y].height_value = noise_sample
			
			# Height
			var type = noise_to_type(noise_sample)
			map[x][y].height_type = type
			if type != ELEVATION_TYPES.DeepestWater && type != ELEVATION_TYPES.DeepWater && type != ELEVATION_TYPES.ShallowWater:
				map[x][y].is_land = true
			
			#Heat
			var heat_sample = (heat_gradient[x][y] * 0.75) / noise_sample
			if map[x][y].is_land:
				if noise_sample < ELEVATION_VALUES.Rock:
					heat_sample += noise_sample * 0.6
			else:
				heat_sample -= WATER_COOLING[type] * 2
			
			heat_sample = clamp(heat_sample, 0.00, 1.00)
			
			map[x][y].heat_value = heat_sample
			
			#Precipitation
			var precip_sample = precip_map.get_pixel(x, y).v
			if map[x][y].is_land:
				if noise_sample < ELEVATION_VALUES.Rock:
					precip_sample -= noise_sample * 0.05
			else:
				precip_sample -= WATER_COOLING[type] * 0.5
			
			precip_sample = clamp(precip_sample, 0.00, 1.00)
			map[x][y].precipitation = precip_sample
			
			#end
			tiles_complete += 1
#			progress_bar.value = stepify(float(tiles_complete) / float(tiles_total) * 100.0, 0.01)
	### END Load Step 3
	
	### Load Step 4
	progress_bar.label.text = "Calculating Heat and Precipitation..."
	for x in range(width):
		for y in range(height):
			#We then check all tiles' neighbors
			map[x][y].check_neighbors(map, width)
			var bitmask_value = map[x][y].update_bitmask()
			
			#Next, we set the colors in our visual image
			var height_color = HEIGHT_COLORS.get(HEIGHT_COLORS.keys()[map[x][y].height_type])
			var heat_color = heat_to_color(map[x][y].heat_value)
			var precip_color = precip_color_gradient.interpolate(map[x][y].precipitation)
			
			
			height_color.v += bitmask_value
			heat_color.v += bitmask_value
			if map[x][y].is_land:
				precip_color.v += bitmask_value
			new_img_height.set_pixel(x,y, height_color)
			new_img_heat.set_pixel(x, y, heat_color)
			new_img_precip.set_pixel(x, y, precip_color)
			
			tiles_complete += 1
#			progress_bar.value = stepify(float(tiles_complete) / float(tiles_total) * 100.0, 0.01)
	
	### END Load Step 4
	
	### Load Step 5
	progress_bar.label.text = "Finishing up..."
	img_tex_height.create_from_image(new_img_height, 1)
	img_tex_heat.create_from_image(new_img_heat, 1)
	img_tex_precip.create_from_image(new_img_precip, 1)
	new_img_height.unlock()
	new_img_heat.unlock()
	new_img_precip.unlock()
	height_map_node.rect_size = Vector2(width, height)
	height_map_node.rect_min_size = height_map_node.rect_size
	height_map_node.texture = img_tex_height
	height_map_node.rect_scale = image_scale
	heat_map_node.rect_size = Vector2(width, height)
	heat_map_node.rect_min_size = heat_map_node.rect_size
	heat_map_node.texture = img_tex_heat
	heat_map_node.rect_scale = image_scale
	precip_map_node.rect_size = Vector2(width, height)
	precip_map_node.rect_min_size = precip_map_node.rect_size
	precip_map_node.texture = img_tex_precip
	precip_map_node.rect_scale = image_scale
	if !height_map_node.visible:
		height_map_node.visible = true
	if !heat_map_node.visible:
		heat_map_node.visible = true
#	vis2.rect_position.x = float(width) * vis2.rect_scale.x
#	vis3.rect_position.x = float(width * 2) * vis3.rect_scale.x
#	vis2.rect_position.x = 0
#	vis3.rect_position.x = 0
	
	tiles_complete += width * height
#	progress_bar.value = stepify(float(tiles_complete) / float(tiles_total) * 100.0, 0.01)
	### END Load Step 5
	
	progress_bar.fade_out()
	$Generate.enable()
	height_map_node.show()
	heat_map_node.show()
	precip_map_node.show()
	switch_mode_button.show()
	
	#end
	generating = false
	_thread.wait_to_finish()
	pass



func create_2d_array(width: int, height: int):
	
	
	var a = []
	
	for y in range(height):
		a.append([])
		a[y].resize(width)
	
		for x in range(width):
			a[y][x] = Tile.new()
			a[y][x].x = y
			a[y][x].y = x
			a[y][x].is_land = false
			tiles_complete += 1
#			progress_bar.value = stepify(tiles_complete / tiles_total * 100.0, 0.01)
#			progress_bar.update()
	
	return a

func heat_to_color(value):
	return HEAT_COLORS.High.linear_interpolate(HEAT_COLORS.Low, value)
	pass


func create_voronoi_diagram(imgSize : Vector2, num_cells: int):
	
	var img = Image.new()
	img.create(imgSize.x, imgSize.y, false, Image.FORMAT_RGBH)
	img.lock()
	
	var points = []
	var colors = []
	
	for i in range(num_cells):
		points.push_back(Vector2(int(randf()*img.get_size().x), int(randf()*img.get_size().y)))
		
		randomize()
		var colorPossibilities = [ Color.blue, Color.red, Color.green, Color.purple, Color.yellow, Color.orange]
		colors.push_back(colorPossibilities[randi()%colorPossibilities.size()])
		
	for y in range(img.get_size().y):
		for x in range(img.get_size().x):
			var dmin = img.get_size().length()
			var j = -1
			for i in range(num_cells):
				var d = (points[i] - Vector2(x, y)).length()
				if d < dmin:
					dmin = d
					j = i
			img.set_pixel(x, y, colors[j])
	
	img.unlock()
	var texture = ImageTexture.new()
	texture.create_from_image(img)
	return texture

#func clear_map(map):
##	for x in map.size():
##		for y in map[x].size():
###			if is_instance_valid(map[x][y]):
##			map[x][y].free()
#			tiles_complete += 1
##			progress_bar.value = stepify(tiles_complete / tiles_total * 100.0, 0.01)
##			else:
##				map[x][y] = null
#			pass
#	map.clear()
#	pass

func shrink_2d_array(array, scale):
	var a = []
	for x in range(array.size() / scale):
		a.append([])
		a[x].resize(array.size() / scale)

		for y in range(array.size() / scale):
			a[x][y] = 0.0
	
	var xx = 0
	var yy = 0
	for x in (a.size()):
		for y in (a.size()):
			a[x][y] = array[xx][yy]
			
			if y == 0:
				yy = 0
			else:
				yy += scale
			pass
		#end x
		if x == 0:
			xx = 0
		else:
			xx += scale
	return a
	pass

func expand_2d_array(array, scale):
	var a = []
	var x = 0
	var y = 0
	while x < array.size() - 1:
		a.append([])
		while y < x.size() - 1:
			a[x][y] = array[x][y]
			
			#end y
			y += scale
			pass
		#end x
		x += scale
	return a
	pass

func rgb(r: int, g: int, b: int, a: int = 255):
	var color = Color(0.0,0.0,1.0)
	color.r8 = r
	color.g8  = g
	color.b8 = b
	color.a8 = a
	return color
	pass

func list_files_in_directory(path):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
	
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with(".") && not file.ends_with(".import"):
			files.append(file)
	
	dir.list_dir_end()
	
	return files

func switch_mode_pressed() -> void:
	if mode == 0:
		switch_mode_button.text = "Side By Side Mode"
		side_by_side_container.remove_child(height_map_node)
		side_by_side_container.remove_child(heat_map_node)
		side_by_side_container.remove_child(precip_map_node)
		overlay_container.add_child(height_map_node)
		overlay_container.add_child(heat_map_node)
		overlay_container.add_child(precip_map_node)
		height_map_node.rect_position = Vector2()
		heat_map_node.rect_position = Vector2()
		precip_map_node.rect_position = Vector2()
		height_map_node.get_child(1).value = 1.0
		heat_map_node.get_child(1).value = 0.333
		precip_map_node.get_child(1).value = 0.333
		height_map_node.get_child(1).show()
		heat_map_node.get_child(1).show()
		precip_map_node.get_child(1).show()
		heat_map_node.self_modulate.a = 0.333
		precip_map_node.self_modulate.a = 0.333
		height_map_node.get_child(0).hide()
		heat_map_node.get_child(0).hide()
		precip_map_node.get_child(0).hide()
		mode = 1
	else:
		switch_mode_button.text = "Overlay Mode"
		overlay_container.remove_child(height_map_node)
		overlay_container.remove_child(heat_map_node)
		overlay_container.remove_child(precip_map_node)
		side_by_side_container.add_child(height_map_node)
		side_by_side_container.add_child(heat_map_node)
		side_by_side_container.add_child(precip_map_node)
		height_map_node.self_modulate.a = 1.0
		heat_map_node.self_modulate.a = 1.0
		precip_map_node.self_modulate.a = 1.0
		height_map_node.get_child(0).show()
		heat_map_node.get_child(0).show()
		precip_map_node.get_child(0).show()
		height_map_node.get_child(1).hide()
		heat_map_node.get_child(1).hide()
		precip_map_node.get_child(1).hide()
		mode = 0
		pass
	pass # Replace with function body.
