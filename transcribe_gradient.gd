extends TextureRect

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var export_array: Array = []

var generating: bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	_on_Generate()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func transcribe_gradient(_thread):
	var file = File.new()
	for i in list_files_in_directory("res://image_samples//image/gradient"):
		var img = load("res://image_samples/image/gradient/" + i).get_data()
		var tex_name = i.replace(".png", "")
		img.lock()
		export_array = create_2d_array(img.get_width(), img.get_height())
		for x in range(0, img.get_width()):
			for y in range(0, img.get_height()):
				export_array[x][y] = stepify(img.get_pixel(x, y).gray(), 0.001)
		img.unlock()
		file.open("res://image_samples/data/" + tex_name + ".txt", File.WRITE_READ)
		file.store_var(export_array)
		file.close()
		generating = false
	
	_thread.call_deferred("wait_to_finish")
	
	get_tree().call_deferred("quit")
	
	pass

func _on_Generate():
	var thread = Thread.new()
	thread.start(self, "transcribe_gradient", thread)
	pass # Replace with function body.


func create_2d_array(width, height, value = 0.0):
	var a = []
	
	for y in range(height):
		a.append([])
		a[y].resize(width)
	
		for x in range(width):
			a[y][x] = value
	
	return a

func list_files_in_directory(path):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
	
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with(".") && not file.ends_with(".import"):
			files.append(file)
	
	dir.list_dir_end()
	
	return files
