extends Node
class_name _Logger

const LOG_FOLDER_NAME = "_logs"
const LOG_EXTENSION = ".log"

var log_file = File.new()
var log_file_path:String
var current_log:Array = []
var _helper

func _init(helper:Node):
	_helper = helper
	if OS.has_feature("editor"): log_file_path = ProjectSettings.globalize_path("res://") + LOG_FOLDER_NAME
	else:log_file_path = OS.get_executable_path().get_base_dir() + "/" + LOG_FOLDER_NAME

func _log(var message, single_log:bool=true) -> void:
	if not message is String: message = String(message)
	current_log.append(message)
	if single_log:
		write_log()
	pass

func write_log() -> void:
	if current_log.empty(): return
	check_log_directory()
	var new_log:String = ""
	var log_time:String = _helper.get_current_time()
	for i in current_log.size():
		if i > 0: new_log += "\n-----\n"
		new_log += current_log[i]
	current_log.clear()
	log_file.open(log_file_path + "/" + log_time + LOG_EXTENSION, File.WRITE)
	log_file.store_string(new_log)
	log_file.close()
	pass

func check_log_directory() -> void:
	var dir:Directory = Directory.new()
	if !dir.dir_exists(log_file_path): dir.make_dir(log_file_path)
	pass
