extends Node

var Logger:_Logger = _Logger.new(self)

enum TIME_FORMAT {
	DATE = 0,
	TIME = 1,
	FULL = 2
}
enum LOG_LEVEL{
	LOG=0,
	WARNING=1,
	ERROR=2
}
const ERR_STRING = [
	"FAILED = 1 — Generic error.",
	"ERR_UNAVAILABLE = 2 — Unavailable error.",
	"ERR_UNCONFIGURED = 3 — Unconfigured error.",
	"ERR_UNAUTHORIZED = 4 — Unauthorized error.",
	"ERR_PARAMETER_RANGE_ERROR = 5 — Parameter range error.",
	"ERR_OUT_OF_MEMORY = 6 — Out of memory (OOM) error.",
	"ERR_FILE_NOT_FOUND = 7 — File: Not found error.",
	"ERR_FILE_BAD_DRIVE = 8 — File: Bad drive error.",
	"ERR_FILE_BAD_PATH = 9 — File: Bad path error.",
	"ERR_FILE_NO_PERMISSION = 10 — File: No permission error.",
	"ERR_FILE_ALREADY_IN_USE = 11 — File: Already in use error.",
	"ERR_FILE_CANT_OPEN = 12 — File: Can’t open error.",
	"ERR_FILE_CANT_WRITE = 13 — File: Can’t write error.",
	"ERR_FILE_CANT_READ = 14 — File: Can’t read error.",
	"ERR_FILE_UNRECOGNIZED = 15 — File: Unrecognized error.",
	"ERR_FILE_CORRUPT = 16 — File: Corrupt error.",
	"ERR_FILE_MISSING_DEPENDENCIES = 17 — File: Missing dependencies error.",
	"ERR_FILE_EOF = 18 — File: End of file (EOF) error.",
	"ERR_CANT_OPEN = 19 — Can’t open error.",
	"ERR_CANT_CREATE = 20 — Can’t create error.",
	"ERR_QUERY_FAILED = 21 — Query failed error.",
	"ERR_ALREADY_IN_USE = 22 — Already in use error.",
	"ERR_LOCKED = 23 — Locked error.",
	"ERR_TIMEOUT = 24 — Timeout error.",
	"ERR_CANT_CONNECT = 25 — Can’t connect error.",
	"ERR_CANT_RESOLVE = 26 — Can’t resolve error.",
	"ERR_CONNECTION_ERROR = 27 — Connection error.",
	"ERR_CANT_ACQUIRE_RESOURCE = 28 — Can’t acquire resource error.",
	"ERR_CANT_FORK = 29 — Can’t fork process error.",
	"ERR_INVALID_DATA = 30 — Invalid data error.",
	"ERR_INVALID_PARAMETER = 31 — Invalid parameter error.",
	"ERR_ALREADY_EXISTS = 32 — Already exists error.",
	"ERR_DOES_NOT_EXIST = 33 — Does not exist error.",
	"ERR_DATABASE_CANT_READ = 34 — Database: Read error.",
	"ERR_DATABASE_CANT_WRITE = 35 — Database: Write error.",
	"ERR_COMPILATION_FAILED = 36 — Compilation failed error.",
	"ERR_METHOD_NOT_FOUND = 37 — Method not found error.",
	"ERR_LINK_FAILED = 38 — Linking failed error.",
	"ERR_SCRIPT_FAILED = 39 — Script failed error.",
	"ERR_CYCLIC_LINK = 40 — Cycling link (import cycle) error.",
	"ERR_INVALID_DECLARATION = 41 — Invalid declaration error.",
	"ERR_DUPLICATE_SYMBOL = 42 — Duplicate symbol error.",
	"ERR_PARSE_ERROR = 43 — Parse error.",
	"ERR_BUSY = 44 — Busy error.",
	"ERR_SKIP = 45 — Skip error.",
	"ERR_HELP = 46 — Help error.",
	"ERR_BUG = 47 — Bug error.",
	"ERR_PRINTER_ON_FIRE = 48 — Printer on fire error. (This is an easter egg, no engine methods return this error code.)",
]
const NULL:int = -934573562345902875
const KEYS:Dictionary = {
	"ESCAPE":16777217, # Escape key
	"TAB":16777218, # Tab key
	"BACKTAB":16777219, # Shift+Tab key
	"BACKSPACE":16777220, # Backspace key
	"ENTER":16777221, # Return key (on the main keyboard).
	"KP_ENTER":16777222, # Enter key on the numeric keypad.
	"INSERT":16777223, # Insert key
	"DELETE":16777224, # Delete key
	"PAUSE":16777225, # Pause key
	"PRINT":16777226, # Print Screen key
	"HOME":16777229, # Home key
	"END":16777230, # End key
	"LEFT":16777231, # Left arrow key
	"UP":16777232, # Up arrow key
	"RIGHT":16777233, # Right arrow key
	"DOWN":16777234, # Down arrow key
	"PAGEUP":16777235, # Page Up key
	"PAGEDOWN":16777236, # Page Down key
	"SHIFT":16777237, # Shift key
	"CONTROL":16777238, # Control key
	"ALT":16777240, # Alt key
	"F1":16777244, # F1 key
	"F2":16777245, # F2 key
	"F3":16777246, # F3 key
	"F4":16777247, # F4 key
	"F5":16777248, # F5 key
	"F6":16777249, # F6 key
	"F7":16777250, # F7 key
	"F8":16777251, # F8 key
	"F9":16777252, # F9 key
	"F10":16777253, # F10 key
	"F11":16777254, # F11 key
	"F12":16777255, # F12 key
	"F13":16777256, # F13 key
	"F14":16777257, # F14 key
	"F15":16777258, # F15 key
	"F16":16777259, # F16 key
	"KEY_KP_MULTIPLY":16777345, #Multiply (*) key on the numeric keypad.
	"KEY_KP_DIVIDE":16777346, #Divide (/) key on the numeric keypad.
	"KEY_KP_SUBTRACT":16777347, #Subtract (-) key on the numeric keypad.
	"KEY_KP_PERIOD":16777348, #Period (.) key on the numeric keypad.
	"KEY_KP_ADD":16777349,
	"KP_0":16777350, # Number 0 on the numeric keypad.
	"KP_1":16777351, # Number 1 on the numeric keypad.
	"KP_2":16777352, # Number 2 on the numeric keypad.
	"KP_3":16777353, # Number 3 on the numeric keypad.
	"KP_4":16777354, # Number 4 on the numeric keypad.
	"KP_5":16777355, # Number 5 on the numeric keypad.
	"KP_6":16777356, # Number 6 on the numeric keypad.
	"KP_7":16777357, # Number 7 on the numeric keypad.
	"KP_8":16777358, # Number 8 on the numeric keypad.
	"KP_9":16777359, # Number 9 on the numeric keypad.
	"SPACE":32, # Space key
	"EXCLAM":33, # ! key
	"QUOTEDBL":34, # ” key
	"NUMBERSIGN":35, # # key
	"DOLLAR":36, # $ key
	"PERCENT":37, # % key
	"AMPERSAND":38, # & key
	"APOSTROPHE":39, # ‘ key
	"PARENLEFT":40, # ( key
	"PARENRIGHT":41, # ) key
	"ASTERISK":42, # * key
	"PLUS":43, # + key
	"COMMA":44, # , key
	"MINUS":45, # - key
	"PERIOD":46, # . key
	"SLASH":47, # / key
	"0":48, # Number 0.
	"1":49, # Number 1.
	"2":50, # Number 2.
	"3":51, # Number 3.
	"4":52, # Number 4.
	"5":53, # Number 5.
	"6":54, # Number 6.
	"7":55, # Number 7.
	"8":56, # Number 8.
	"9":57, # Number 9.
	"COLON":58, # : key
	"SEMICOLON":59, # ; key
	"LESS":60, # < key
	"EQUAL":61, #"":key
	"GREATER":62, # > key
	"QUESTION":63, # ? key
	"AT":64, # @ key
	"A":65, # A key
	"B":66, # B key
	"C":67, # C key
	"D":68, # D key
	"E":69, # E key
	"F":70, # F key
	"G":71, # G key
	"H":72, # H key
	"I":73, # I key
	"J":74, # J key
	"K":75, # K key
	"L":76, # L key
	"M":77, # M key
	"N":78, # N key
	"O":79, # O key
	"P":80, # P key
	"Q":81, # Q key
	"R":82, # R key
	"S":83, # S key
	"T":84, # T key
	"U":85, # U key
	"V":86, # V key
	"W":87, # W key
	"X":88, # X key
	"Y":89, # Y key
	"Z":90, # Z key
	"BRACKETLEFT":91, # [ key
	"BACKSLASH":92, # \ key
	"BRACKETRIGHT":93, # ] key
	"ASCIICIRCUM":94, # ^ key
	"UNDERSCORE":95, # _ key
	"QUOTELEFT":96, # ` key
	"BRACELEFT":123, # { key
	"BAR":124, # | key
	"BRACERIGHT":125, # } key
	"ASCIITILDE":126, # ~ key
}
var ms_in_window:bool = true setget _set_mouse_in_window, is_mouse_in_window
var exec_dir:String

onready var ctrl_h:Control = Control.new()

func _init():
	printerr()
	if is_running_in_editor(): exec_dir = ProjectSettings.globalize_path("res://")
	else: exec_dir = OS.get_executable_path().get_base_dir() + "/"
	pass

func _ready():
	add_child(ctrl_h)
	ctrl_h.rect_position = Vector2()
	ctrl_h.rect_size = Vector2()
	set_process(false)
	set_process_input(false)

func _notification(what:int):
	match what:
		NOTIFICATION_WM_MOUSE_ENTER: ms_in_window = true
		NOTIFICATION_WM_MOUSE_EXIT: ms_in_window = false
	pass

# Custom types and such
func Array2D(size:Vector2, value=int(-1), callback_object: Object = null,
			 callback_function: String = ""):
	var c_value = null
	var a = []
	for x in range(size.x):
		a.append([])
		a[x].resize(size.y)
		for y in range(size.y):
			if value is GDScript:
				c_value = value.new()
			elif value is Node:
				c_value = value.instance()
			elif value is Array:
				c_value = value.duplicate(true)
			else:
				c_value = value
			
			a[x][y] = c_value
			
			if (callback_object and
				callback_function != "" and
				callback_object.has_method(callback_function)):
				callback_object.call(callback_function, c_value, x, y)
	return a
	pass
# End Custom Types

func pos_to_1d(x: int, y: int, width: int) -> int: return x * width + y

func abs_i(value: int) -> int:
	if value < 0: return -value
	else: return value

func _compare_aabbs(a:AABB, b:AABB) -> bool:
	if !_compare_vectors(a.position, b.position): return false
	if !_compare_vectors(a.size, b.size): return false
	if !_compare_vectors(a.end, b.end): return false
	return true

func _compare_arrays(a:Array, b:Array) -> bool:
	if a.size() != b.size(): return false
	for i in a.size():
		var value = a[i]
		if !b.has(value): return false
		if !compare_vars(value, b[i]): return false
	return true
	pass

func _compare_basis(a:Basis, b:Basis) -> bool:
	if !_compare_vectors(a.x, b.x): return false
	if !_compare_vectors(a.y, b.y): return false
	if !_compare_vectors(a.z, b.z): return false
	return true
	pass

func _compare_colors(a:Color, b:Color) -> bool:
	if a.to_argb64() != b.to_argb64(): return false
	else: return true
	pass

func _compare_dictionaries(a:Dictionary, b:Dictionary) -> bool:
	var a_keys:Array = a.keys()
	var b_keys:Array = b.keys()
	if a_keys.size() != b_keys.size(): return false
	for i in a_keys.size():
		var value = a[a_keys[i]]
		if !b.has(a_keys[i]): return false
		if !compare_vars(value, b[a_keys[i]]): return false
	return true
	pass

func _compare_floats(a:float, b:float, epsilon:float=0.00001) -> bool:
	return abs(a - b) <= epsilon
	pass

func _compare_planes(a:Plane, b:Plane) -> bool:
	if !_compare_floats(a.d, b.d): return false
	if !_compare_vectors(a.normal, b.normal): return false
	if !_compare_floats(a.x, b.x): return false
	if !_compare_floats(a.y, b.y): return false
	if !_compare_floats(a.z, b.z): return false
	return true
	pass

func _compare_quats(a:Quat, b:Quat) -> bool:
	if !_compare_floats(a.w, b.w): return false
	if !_compare_floats(a.x, b.x): return false
	if !_compare_floats(a.y, b.y): return false
	if !_compare_floats(a.z, b.z): return false
	return true
	pass

func _compare_transform2ds(a:Transform2D, b:Transform2D) -> bool:
	if !_compare_vectors(a.origin, b.origin): return false
	if !_compare_vectors(a.x, b.x): return false
	if !_compare_vectors(a.y, b.y): return false
	return true
	pass

func _compare_transforms(a:Transform, b:Transform) -> bool:
	if !_compare_basis(a.basis, b.basis): return false
	if !_compare_vectors(a.origin, b.origin): return false
	return true
	pass

func _compare_vectors(var a, var b) -> bool:
	if !a is Vector2 && !a is Vector3: return false
	if !b is Vector2 && !b is Vector3: return false
	if a is Vector2 and !b is Vector2: return false
	if a is Vector3 and !b is Vector3: return false
	if !_compare_floats(a.x, b.x): return false
	if !_compare_floats(a.y, b.y): return false
	if a is Vector3:
		if _compare_floats(a.z, b.z): return false
	return true
	pass

func compare_vars(var a, var b, log_diff:bool=false) -> bool:
	if typeof(a) != typeof(b): return false
	if _is_compareable(a):
		if a != b:
			if log_diff:
				Logger._log(a, false)
				Logger._log(b)
			return false
	elif _is_array(a):
		if !_compare_arrays(a, b):
			if log_diff:
				Logger._log(a, false)
				Logger._log(b)
			return false
	else:
		match typeof(a):
			TYPE_AABB:
				if !_compare_aabbs(a, b):
					if log_diff:
						Logger._log(a, false)
						Logger._log(b)
					return false
			TYPE_BASIS:
				if !_compare_basis(a, b):
					if log_diff:
						Logger._log(a, false)
						Logger._log(b)
					return false
			TYPE_COLOR:
				if !_compare_colors(a, b):
					if log_diff:
						Logger._log(a, false)
						Logger._log(b)
					return false
			TYPE_NIL:
				if typeof(b) != TYPE_NIL:
					if log_diff:
						Logger._log(a, false)
						Logger._log(b)
					return false
			TYPE_PLANE:
				if !_compare_planes(a, b):
					if log_diff:
						Logger._log(a, false)
						Logger._log(b)
					return false
			TYPE_QUAT:
				if !_compare_quats(a, b):
					if log_diff:
						Logger._log(a, false)
						Logger._log(b)
					return false
			TYPE_REAL:
				if !_compare_floats(a, b):
					if log_diff:
						Logger._log(a, false)
						Logger._log(b)
					return false
			TYPE_VECTOR2:
				if !_compare_vectors(a, b):
					if log_diff:
						Logger._log(a, false)
						Logger._log(b)
					return false
			TYPE_VECTOR3:
				if !_compare_vectors(a, b):
					if log_diff:
						Logger._log(a, false)
						Logger._log(b)
					return false
			TYPE_TRANSFORM2D:
				if !_compare_transform2ds(a, b):
					if log_diff:
						Logger._log(a, false)
						Logger._log(b)
					return false
			TYPE_TRANSFORM:
				if !_compare_transforms(a, b):
					if log_diff:
						Logger._log(a, false)
						Logger._log(b)
					return false
	return true
	pass

func _is_compareable(what) -> bool:
	if (
		typeof(what) == TYPE_NIL
		|| typeof(what) == TYPE_BOOL
		|| typeof(what) == TYPE_INT
		|| typeof(what) == TYPE_STRING
		|| typeof(what) == TYPE_NODE_PATH
		|| typeof(what) == TYPE_RID
		|| typeof(what) == TYPE_OBJECT
	): pass
	return true
	pass

func _is_array(var what) -> bool:
	if (
		typeof(what) == TYPE_ARRAY
		|| typeof(what) == TYPE_RAW_ARRAY
		|| typeof(what) == TYPE_INT_ARRAY
		|| typeof(what) == TYPE_REAL_ARRAY
		|| typeof(what) == TYPE_STRING_ARRAY
		|| typeof(what) == TYPE_VECTOR2_ARRAY
		|| typeof(what) == TYPE_COLOR_ARRAY
	): return true
	else: return false
	pass

func floor_vector(var value):
	if value is Vector2:
		return Vector2(floor(value.x), floor(value.y))
	elif value is Vector3:
		return Vector3(floor(value.x), floor(value.y), floor(value.z))
	else:
		push_error("Helper::ceil_vector: Parameter is not a 'Vector2' or 'Vector3'")
	pass

func ceil_vector(var value):
	if value is Vector2:
		return Vector2(ceil(value.x), ceil(value.y))
	elif value is Vector3:
		return Vector3(ceil(value.x), ceil(value.y), ceil(value.z))
	else:
		push_error("Helper::ceil_vector: Parameter is not a 'Vector2' or 'Vector3'")
	pass

func clamp_vector(var value, var _min, var _max):
	if (value is Vector2 and _min is Vector2 and _max is Vector2):
		return _clamp_vector2(value, _min, _max)
	elif (value is Vector2 and _min is Vector2 and _max is int):
		if value.x < _min.x: value.x = _min.x
		if value.y < _min.y: value.y = _min.y
		return value
	elif (value is Vector3 and _min is Vector3 and _max is Vector3):
		return _clamp_vector3(value, _min, _max)
	elif (value is Vector3 and _min is Vector3 and _max is int):
		if value.x < _min.x: value.x = _min.x
		if value.y < _min.y: value.y = _min.y
		if value.x < _min.z: value.z = _min.z
		return value
	else:
		push_error("Helper::clamp_vector: Parameters do not match 'Vector2' or 'Vector3'")
	pass

func _clamp_vector2(value:Vector2, _min:Vector2, _max:Vector2) -> Vector2:
	return Vector2(clamp(value.x, _min.x, _max.x), clamp(value.y, _min.y, _max.y))

func _clamp_vector3(value:Vector3, _min:Vector3, _max:Vector3) -> Vector3:
	return Vector3(
		clamp(value.x, _min.x, _max.x),
		clamp(value.y, _min.y, _max.y),
		clamp(value.z, _min.z, _max.z))

func clamp_integer(value:int, _min:int = NULL, _max:int = NULL) -> int:
	if value < _min and not _min == NULL: return _min
	if value > _max and not _max == NULL: return _max
	return value
	pass

func create_2d_array(width:int, height:int, value=null) -> Array:
	var arr = []
	for x in range(width):
		arr.append([])
		arr[x].resize(height)
		for y in range(height):
			if value is Object: arr[x][y] = value.new()
			else: arr[x][y] = value
	return arr


func dictionary_to_string(data:Dictionary) -> String:
	var string := "{"
	var keys = data.keys()
	var values = data.values()
	for i in keys.size():
		string += "\n\t" + String(keys[i]) + "="
		match typeof(values[i]):
			TYPE_ARRAY:
				string += _process_array(values[i], 2)
			TYPE_BOOL:
				string += String(values[i])
			TYPE_DICTIONARY:
				string += _process_dictionary(values[i], 2)
			TYPE_INT:
				string += String(values[i])
			TYPE_REAL:
				string += String(values[i])
			TYPE_STRING:
				string += values[i]
			TYPE_VECTOR2:
				string += "Vector2(" + String(values[i].x) + ", " + String(values[i].y)
		if i < keys.size() - 1: string += ","
	#end
	string += "\n}"
	return string
	pass

func _process_dictionary(data:Dictionary, indent:int) -> String:
	var string := "{"
	var keys = data.keys()
	var values = data.values()
	for i in keys.size():
		string += "\n" + "\t".repeat(indent) + String(keys[i]) + "="
		match typeof(values[i]):
			TYPE_ARRAY:
				string += _process_array(values[i], indent + 1)
			TYPE_BOOL:
				string += String(values[i])
			TYPE_DICTIONARY:
				string += _process_dictionary(values[i], indent + 1)
			TYPE_INT:
				string += String(values[i])
			TYPE_REAL:
				string += String(values[i])
			TYPE_STRING:
				string += values[i]
			TYPE_VECTOR2:
				string += "Vector2(" + String(values[i].x) + ", " + String(values[i].y)
		if i < keys.size() - 1: string += ","
	#end
	string += "\n" + "\t".repeat(indent - 1) + "}"
	return string
	pass

func _process_array(array:Array, indent:int) -> String:
	var string := "["
	for i in array.size():
		string += "\n" + "\t".repeat(indent)
		
		match typeof(array[i]):
			TYPE_ARRAY:
				string += _process_array(array[i], indent + 1)
			TYPE_BOOL:
				string += String(array[i])
			TYPE_DICTIONARY:
				string += _process_dictionary(array[i], indent + 1)
			TYPE_INT:
				string += String(array[i])
			TYPE_REAL:
				string += String(array[i])
			TYPE_STRING:
				string += array[i]
			TYPE_VECTOR2:
				string += "Vector2(" + String(array[i].x) + ", " + String(array[i].y) + ")"
		if i < array.size() - 1: string += ","
	
	#end
	string += "\n" + "\t".repeat(indent - 1) + "]"
	return string
	pass

func get_correct_node(check_node:Object, group_name:String, is_a_parent_of:=false):
	if not get_tree(): return 
	var _node = false
	
	var objects = get_tree().get_nodes_in_group(group_name)
	if !objects.empty():
		for i in objects:
			if is_a_parent_of:
				if is_node_a_descendant_of(i, check_node):
					_node = i
			else:
				if is_node_a_descendant_of(check_node, i):
					_node = i
		
	if typeof(_node) == TYPE_BOOL:
		var error_text = "Error - "
		error_text += check_node.name + " could not find node in in group '" + group_name + "'"
		if is_a_parent_of: error_text += " that was a parent of " + check_node.name
		push_error(error_text)
		_node = null
	return _node
	pass

func is_node_a_descendant_of(child:Node, parent:Node) -> bool:
	var thread:Thread = Thread.new()
	thread.start(self, "_is_node_a_descendant_of_thread", [child, parent])
	var value = thread.wait_to_finish()
	return value
	pass

func _is_node_a_descendant_of_thread(args:Array) -> bool:
	var child = args[0]
	var parent = args[1]
	var b = false
	var node = child.get_parent()
	while (node != get_tree().root):
		if node == parent:
			b = true
			break
		else: 
			node = node.get_parent()
	return b
	pass

func get_current_time(time_format:int=TIME_FORMAT.FULL) -> String:
	var time:Dictionary = OS.get_datetime()
	var s:String = ""
	match time_format:
		TIME_FORMAT.DATE:
			s = String(time.month) + "m-" + String(time.day) + "d-" + String(time.year) + "y"
		TIME_FORMAT.TIME:
			s = String(time.hour) + "h-" + String(time.minute) + "m-" + String(time.second) + "s"
		TIME_FORMAT.FULL:
			s = String(time.month) + "m-" + String(time.day) + "d-" + String(time.year) + "y_"
			s += String(time.hour) + "h-" + String(time.minute) + "m-" + String(time.second) + "s"
	return s
	pass

func get_time_difference_string(unix_time_a:Dictionary, unix_time_b:Dictionary) -> String:
	var diff := {year = 0, month = 0, day = 0, hour = 0, minute = 0, second = 0}
	var _string = ""
	var ding := false
	for i in diff.keys():
		if unix_time_a[i] != unix_time_b[i]:
			diff[i] = unix_time_b[i] - unix_time_a[i]
	if diff.year != 0:
		if diff.month == 1:
			_string += String(diff.year) + " year"
		else:
			_string += String(diff.year) + " years"
		ding = true
	if diff.month != 0:
		if ding:
			if diff.month == 1:
				_string += String(diff.month) + ", month"
			else:
				_string += String(diff.month) + ", months"
		else:
			if diff.month == 1:
				_string += String(diff.month) + " month"
			else:
				_string += String(diff.month) + " months"
			ding = true
	if diff.day != 0:
		if ding:
			if diff.day == 1:
				_string += String(diff.month) + ", day"
			else:
				_string += String(diff.month) + ", days"
		else:
			if diff.day == 1:
				_string += String(diff.day) + " day"
			else:
				_string += String(diff.day) + " days"
			ding = true
	if diff.hour != 0:
		if ding:
			if diff.hour == 1:
				_string += String(diff.hour) + " hour, "
			else:
				if diff.hour == 1:
					_string += String(diff.hour) + " hour, "
				else:
					_string += String(diff.hour) + " hours, "
		else:
			if diff.hour == 1:
				_string += String(diff.hour) + " hour"
			else:
				_string += String(diff.hour) + " hours"
			ding = true
	if diff.minute != 0:
		if ding:
			if diff.minute == 1:
				_string += String(diff.minute) + " minute, "
			else:
				_string += String(diff.minute) + " minutes, "
		else:
			if diff.minute == 1:
				_string += String(diff.minute) + " minute"
			else:
				_string += String(diff.minute) + " minutes"
			ding = true
	if diff.second != 0:
		if diff.second == 1:
			_string += String(diff.second) + " second"
		else:
			_string += String(diff.second) + " seconds"
	return _string
	pass

func get_error_string(index:int) -> String: return ERR_STRING[index]

func get_executable_directory() -> String: return exec_dir

func get_keycode(key:String) -> int:
	if KEYS.has(key):
		return KEYS[key]
	return -1
	pass

func get_window_rect() -> Rect2:
	return Rect2(OS.window_position, OS.window_size)
	pass

func set_window_rect(rect:Rect2):
	OS.window_position = rect.position
	OS.window_size = rect.size
	pass

func get_mouse_screen_position():
	return ctrl_h.get_viewport_transform() * (ctrl_h.get_global_transform() * ctrl_h.get_local_mouse_position())
	pass

func is_running_in_editor() -> bool:
	return OS.has_feature("editor")

func is_running_in_debug() -> bool:
	return OS.has_feature("debug")

func is_mouse_hovering(area:Vector2, mouse_pos:Vector2, tolerance:float=0.0) -> bool:
	if mouse_pos.x >= 0 - tolerance && mouse_pos.y >= 0 - tolerance:
		if mouse_pos.x < area.x + tolerance && mouse_pos.y < area.y + tolerance:
			return true
	return false
	pass

func is_mouse_in_window() -> bool:
	var r:bool = true
	var win_start = OS.window_position
	var win_end = win_start + OS.window_size
	var pos:Vector2 = get_mouse_screen_position()
	if (
		pos.x < win_start.x
		|| pos.y < win_start.y
		|| pos.x > win_end.x
		|| pos.y > win_end.y
	): r = false
	return r

func _set_mouse_in_window(in_window:bool):
	ms_in_window = in_window
	pass

func _log(message:String, log_level: int = 0, save_to_log_file: bool = false, single_log:bool=true):
	match log_level:
		LOG_LEVEL.LOG:
			print(message)
		LOG_LEVEL.WARNING:
			push_warning(message)
		LOG_LEVEL.ERROR:
			push_error("ERROR | " + message)
	if save_to_log_file:
		Logger._log(message, single_log)

func ms_to_seconds(ms:float) -> float: return ms * 0.001

